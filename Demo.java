import java.util.ArrayList;
import java.util.Scanner;

/**
 * класс Demo запускает игру и считает количество ходов затраченных на уничтожения корабля
 */


public class Demo {
    static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        int numberOfMoves = 0;
        ArrayList<String> localOfShip = new ArrayList<>();
        Ship ship = new Ship();
        init(localOfShip);
        ship.setLocation(localOfShip);
        do{
            System.out.println(ship.checkShip(shot()));
            numberOfMoves++;
        }while (!ship.getLocation().isEmpty());
        System.out.println("количество затраченых ходов - " + numberOfMoves);
    }


    /**
     * @return координаты выстрела
     */

    private static String shot() {
        System.out.println("введите координаты выстрела");
        return scanner.nextLine();

    }

    /**
     * метод инициализации корабля
     * @param arrayList
     */
    public static void init(ArrayList<String> arrayList) {
        arrayList.add(String.valueOf((int) (Math.random() * 7)));
        arrayList.add(String.valueOf(Integer.parseInt(arrayList.get(0)) + 1));
        arrayList.add(String.valueOf(Integer.parseInt(arrayList.get(1)) + 1));

    }
}
