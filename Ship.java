import java.util.*;



public class Ship {

    private ArrayList<String> location;

    public ArrayList<String> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<String> location) {
        this.location = location;
    }

    /**
     * Метод,определяющий состаяние кораля
     *
     * @param shot
     * @return строка,возвращает мимо, если координаты не совпадает,
     *         ранил если координаты совподают с одной из координат
     *         если координаты закончились выводится убит.
     */
    public String checkShip(String shot) {
        String result = "мимо";
        if ( location.indexOf(shot)!=-1) {
            location.remove(location.indexOf(shot));
            result = "ранен";
        }
        if ( location.isEmpty()) {
            result = "убил";
        }
        return result;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "location=" + location +
                '}';
    }
}











